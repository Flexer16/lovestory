---
-- main.lua


package.path = package.path .. ";lib/?/init.lua;lib/?.lua;src/?.lua"


local lovetoys = require "lovetoys"
local Signal = require "hump.signal"


lovetoys.initialize({
    globals = true,
    debug = true
})


require "ecs.prepare_components"


local UiSystem = require "ecs.systems.ui_system"


function love.load()
    signal = Signal.new()

    engine = Engine()

    local ui_system = UiSystem()

    engine:addSystem(ui_system, "update")
    engine:addSystem(ui_system, "draw")
end


function love.update(dt)
    if dt < 1/60 then
        love.timer.sleep(1/60 - dt)
    end

    engine:update(dt)
end


function love.draw()
    engine:draw()
end
