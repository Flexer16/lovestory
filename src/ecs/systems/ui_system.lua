---
-- ui_system.lua


local class = require "middleclass"
local Suit = require "suit"
local Timer = require "hump.timer"


local UiSystem = class("UiSystem", System)

function UiSystem:initialize(data)
    System.initialize(self)

    self.timer = Timer.new()

    self.ui = Suit.new()

    self.story_pannel_height = 200

    self.line = "asdfsdfgdhjtjryruiyryfghjfghfjkfgjghwerqtewqtewtewjkl;jl;hkljkhlvbnmnbvmnvfdtrstreseearafdsafgdshrshfgcgfdfjdcfgjcfgxhgfxdhtfxtxfgxgfxhgrhgfrhx"
    self.current_line = ""
    self.line_position = 1

    self.timer:every(0.02, function() return self:prepare_line() end)

    self.border = 32
end

function UiSystem:update(dt)
    self.ui:Label(
        self.current_line,
        {align = "left"},
        self.border,
        love.graphics.getHeight() - self.story_pannel_height,
        love.graphics.getWidth() - self.border * 2,
        self.story_pannel_height
    )

    self.timer:update(dt)
end

function UiSystem:draw()
    self.ui:draw()
end

function UiSystem:requires()
    return {}
end

function UiSystem:onAddEntity(entity)
    -- body
end

function UiSystem:onRemoveEntity(entity)
    -- body
end

function UiSystem:prepare_line()
    self.line_position = self.line_position + 1

    if self.line_position > #self.line then
        self.line_position = 1

        return false
    end

    self.current_line = string.sub(self.line, 1, self.line_position)
end

return UiSystem
